/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sipproxy.sipLayer;

import java.util.List;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

/**
 *
 * @author thomas
 */
public class listeningPoints {

    private static listeningPoints points;
    private List listenOnPoints;

    public static listeningPoints getInstance() {
        if (points == null) {
            points = new listeningPoints();
        }
        return points;
    }

    private listeningPoints() {
        Properties prop = this.getSipProperties();        
        this.listenOnPoints = new ArrayList<listeningPoint>();        
        listeningPoint point = new listeningPoint(prop.getProperty("IP_ADDRESS"), Integer.parseInt(prop.getProperty("PORT", "5060")), prop.getProperty("PROTOCOL", "UDP"));
        this.listenOnPoints.add(point);
    }

    private Properties getSipProperties() {
        Properties prop = new Properties();
        try {
            BufferedReader in = new BufferedReader(new FileReader("ConfigurationFile"));

            String line = in.readLine();
            do {
                String[] propp = line.split(" ");
                prop.setProperty(propp[0], propp[1]);
                line = in.readLine();
            } while (line != null);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return prop;
    }

    public List getListen() {
        return listenOnPoints;
    }

    public void setListen(List listen) {
        this.listenOnPoints = listen;
    }
    
}
