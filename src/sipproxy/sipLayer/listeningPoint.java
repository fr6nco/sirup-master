/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sipproxy.sipLayer;

/**
 *
 * @author thomas
 */
public class listeningPoint {
    
    String ipAddress;
    int port;
    String protocol;

    public listeningPoint(String ipAddress, int port, String protocol) {
        this.ipAddress = ipAddress;
        this.port = port;
        this.protocol = protocol;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
}
