/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sipproxy.sipLayer.core;

import java.util.ArrayList;

/**
 *
 * @author Thomas
 */
public class dialog {

    int id;
    String callid;
    String fromTag;
    String toTag;
    String type;
    long startedOn;

    public static final int RINGING = 1;
    public static final int CALL_IN_PROGESS = 2;
    public static final int BYE_SENT = 3;
    public static final int CALL_ENDED = 4;    
    public static final int BUSY_HERE_SENT = 5;
    public static final int UNSUCCESSFUL_BUSY = 6;
    public static final int CANCEL_SENT = 7;
    public static final int UNSUCCESSFUL_CANCEL = 8;
    public static final int NOT_FOUND = 9;
    int dialogState=100;
    
    String branch;

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public int getDialogState() {
        return dialogState;
    }

    public void setDialogState(int dialogState) {
        this.dialogState = dialogState;
    }
    ArrayList<String> messages;
    ArrayList<transaction> transactions;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCallid() {
        return callid;
    }

    public void setCallid(String callid) {
        this.callid = callid;
    }

    public String getFromTag() {
        return fromTag;
    }

    public void setFromTag(String fromTag) {
        this.fromTag = fromTag;
    }

    public String getToTag() {
        return toTag;
    }

    public void setToTag(String toTag) {
        this.toTag = toTag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getStartedOn() {
        return startedOn;
    }

    public void setStartedOn(long startedOn) {
        this.startedOn = startedOn;
    }

    public dialog(String callid, String fromTag, String toTag, String type) {
        this.messages = new ArrayList<String>();
        this.transactions = new ArrayList<transaction>();
        this.callid = callid;
        this.fromTag = fromTag;
        this.toTag = toTag;
        this.type = type;
    }

    public void addTransaction(transaction tran) {
        transactions.add(tran);
    }

    public ArrayList<transaction> getTransactions() {
        return this.transactions;
    }

    public void addMessage(String message) {
        this.messages.add(message);
    }

    public ArrayList<String> getMessages() {
        return this.messages;
    }
}
