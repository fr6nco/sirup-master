/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sipproxy.sipLayer.core;

import java.util.ArrayList;

/**
 *
 * @author Thomas
 */
public class transaction {
    int id;
    String from;
    String to;
    String cseq;
    int cseqNum;
    String branch;
    String transactionSource;
            
    
    long time;
    
    static final String serverTransaction = "Server Transactoin";
    static final String clientTransaction = "Client Transaction";
    
    ArrayList<String> message;

    public int getCseqNum() {
        return cseqNum;
    }

    public void setCseqNum(int cseqNum) {
        this.cseqNum = cseqNum;
    }

        
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCseq() {
        return cseq;
    }

    public void setCseq(String cseq) {
        this.cseq = cseq;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public ArrayList<String> getMessage() {
        return message;
    }

    public void setMessage(ArrayList<String> message) {
        this.message = message;
    }

    public String getTransactionSource() {
        return transactionSource;
    }

    public void setTransactionSource(String transactionSource) {
        this.transactionSource = transactionSource;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
    
    public transaction(String type, String from, String to, String cseq, int cseqNum){
        this.message = new ArrayList<String>();
        this.from = from;
        this.to = to;
        this.cseq = cseq;
        this.cseqNum = cseqNum;
    }
    
    public void addMessage(String message){
        this.message.add(message);
    }
    
    public ArrayList<String> getMessages(){
        return this.message;
    }
    
}
