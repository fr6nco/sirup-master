package sipproxy.sipLayer.core;

import java.security.*;
import java.util.*;

import javax.sip.message.*;
import javax.sip.header.*;
import javax.sip.address.*;

public class DigestServerAuthenticationMethod {

    private String defaultRealm;
    private final Random random;
    private final Hashtable<String, MessageDigest> algorithms;

    /**
     * Default constructor.
     *
     * @param defaultRealm Realm to use when realm part is not specified in
     * authentication headers.
     * @param algorithms List of algorithms that can be used in authentication.
     * @throws NoSuchAlgorithmException If one of algorithms specified in
     * <i>algorithms</i> is not realized in current Java version.
     */
    
    public DigestServerAuthenticationMethod(String defaultRealm, String[] algorithms) throws NoSuchAlgorithmException {
        this.defaultRealm = defaultRealm;

        this.algorithms = new Hashtable<String, MessageDigest>();
        random = new Random(System.currentTimeMillis());

        for (String algorithm : algorithms) {
            this.algorithms.put(algorithm, MessageDigest.getInstance(algorithm));
        }
    }

    /**
     * @return The default realm that is to be used when no domain is specified
     * in authentication headers.
     */
    public String getDefaultRealm() {
        return defaultRealm;
    }

    /**
     * @return The algorithm that is to be used when no algorithm is specified
     * in authentication headers.
     */
    public String getPreferredAlgorithm() {
        return "MD5";
    }

    /**
     * Generate the challenge string.
     *
     * @param algorithm Encryption algorithm. "MD5", for example.
     * @return a generated nonce. Empty string if specified <i>algorithm</i> is
     * not recognized.
     */
    public String generateNonce(String algorithm) {
        MessageDigest messageDigest = algorithms.get(algorithm);
        if (messageDigest == null) {
            return "";
        }

        // Get the time of day and run MD5 over it.
        long time = System.currentTimeMillis();
        long pad = random.nextLong();
        String nonceString = (new Long(time)).toString() + (new Long(pad)).toString();
        byte mdbytes[] = messageDigest.digest(nonceString.getBytes());

        return bytesToHex(mdbytes);
    }

    public String genereteTag() {
        MessageDigest messageDigest = algorithms.get("MD5");
      
        long time = System.currentTimeMillis();
        long pad = random.nextLong();
        String nonceString = (new Long(time)).toString() + (new Long(pad)).toString();
        byte mdbytes[] = messageDigest.digest(nonceString.getBytes());

        return bytesToHex(mdbytes);
    }
    
    public String generateBranch(){
        MessageDigest messageDigest = algorithms.get("MD5");
        
        long time = System.currentTimeMillis();
        long pad = random.nextLong();
        String nonceString = (new Long(time)).toString() + (new Long(pad)).toString();
        
        byte mdbytes[] = messageDigest.digest(nonceString.getBytes());
        String BranchBeginning = "z9hG4bK";
        
        return BranchBeginning.concat(bytesToHex(mdbytes));
    }

    public String bytesToHex(byte[] bytes) {
        final char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }

        return new String(hexChars);
    }

    public boolean doAuthenticate(Request request, AuthorizationHeader authHeader, String user, String password) {
        String username = authHeader.getUsername();
        if (!username.equals(user)) {
            return false;
        }

        String realm = authHeader.getRealm();
        if (realm == null) {
            realm = defaultRealm;
        }

        URI uri = authHeader.getURI();
        if (uri == null) {
            return false;
        }

        String algorithm = authHeader.getAlgorithm();
        if (algorithm == null) {
            algorithm = getPreferredAlgorithm();
        }

        MessageDigest messageDigest = algorithms.get(algorithm);

        if (messageDigest == null) {
            return false;
        }

        byte mdbytes[];

        String A1 = username + ":" + realm + ":" + password;
        String A2 = request.getMethod().toUpperCase() + ":" + uri.toString();
        mdbytes = messageDigest.digest(A1.getBytes());
        String HA1 = bytesToHex(mdbytes);

        mdbytes = messageDigest.digest(A2.getBytes());
        String HA2 = bytesToHex(mdbytes);

        String KD = HA1 + ":" + authHeader.getNonce() + ":" + HA2;
        mdbytes = messageDigest.digest(KD.getBytes());

        String mdString = bytesToHex(mdbytes);
        String response = authHeader.getResponse();

//        System.out.println("response: " + response);
//        System.out.println("our computation: " + mdString);

        return mdString.compareTo(response) == 0;
    }
}
