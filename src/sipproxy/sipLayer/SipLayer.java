/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sipproxy.sipLayer;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.TooManyListenersException;
import java.util.logging.Level;
import java.util.Properties;
import java.util.logging.Logger;
import javax.sip.*;
import javax.sip.address.*;
import javax.sip.header.ContactHeader;
import javax.sip.header.HeaderFactory;
import javax.sip.header.ToHeader;
import javax.sip.header.ViaHeader;
import javax.sip.message.MessageFactory;
import javax.sip.message.Request;
import javax.sip.message.Response;
import javax.sip.SipStack;
import org.apache.log4j.*;
import gov.nist.javax.sip.RequestEventExt;
import java.text.ParseException;
import java.util.List;
import javax.sip.header.CSeqHeader;
import javax.sip.header.CallIdHeader;
import javax.sip.header.ExpiresHeader;
import javax.sip.header.FromHeader;
import javax.sip.header.MaxForwardsHeader;
import sipproxy.sipLayer.core.dialog;
import sipproxy.sipLayer.core.DigestServerAuthenticationMethod;
import sipproxy.manager.manager;

/**
 *
 * @author Thomas
 */
public class SipLayer implements SipListener {

    private static SipLayer sip;
    private manager man;
    private listeningPoints listeningOn;
    private SipStack sipStack;
    private SipFactory sipFactory;
    private AddressFactory addressFactory;
    private HeaderFactory headerFactory;
    private MessageFactory messageFactory;
    private SipProvider sipProvider;
    private ArrayList<dialog> dialogs;
    private ArrayList<String> branches;
    private ArrayList<String> callid;
    private DigestServerAuthenticationMethod dsam;

    public ArrayList<dialog> getDialogs() {
        return dialogs;
    }

    public static SipLayer getInstance() throws
            PeerUnavailableException, TransportNotSupportedException, InvalidArgumentException,
            ObjectInUseException, TooManyListenersException, NoSuchAlgorithmException {
        if (sip == null) {
            sip = new SipLayer();

        }
        return sip;
    }

    public SipLayer() throws
            PeerUnavailableException, TransportNotSupportedException,
            InvalidArgumentException, ObjectInUseException,
            TooManyListenersException, NoSuchAlgorithmException {

        this.branches = new ArrayList<>();
        this.callid = new ArrayList<>();
        dialogs = new ArrayList<dialog>();

        dsam = new DigestServerAuthenticationMethod("SIRUPmanager", new String[]{"MD5"});

        man = manager.getInstance();

        sipFactory = sipFactory.getInstance();

        Properties properties = new Properties();

        properties.setProperty("javax.sip.STACK_NAME", "SipProxy");

//        properties.setProperty("javax.sip.IP_ADDRESS", ip);

//        properties.setProperty("gov.nist.javax.sip.DEBUG_LOG", "log");

//        properties.setProperty("javax.sip.AUTOMATIC_DIALOG_SUPPORT", "off");

//        properties.setProperty("gov.nist.javax.sip.PASS_INVITE_NON_2XX_ACK_TO_LISTENER", "true");

        sipStack = sipFactory.createSipStack(properties);

        headerFactory = sipFactory.createHeaderFactory();

        addressFactory = sipFactory.createAddressFactory();

        messageFactory = sipFactory.createMessageFactory();

        this.listeningOn = listeningPoints.getInstance();
        listeningPoint point = (listeningPoint) listeningOn.getListen().get(0);

        ListeningPoint listenStack = sipStack.createListeningPoint(point.getIpAddress(), point.getPort(), point.getProtocol());

        sipProvider = sipStack.createSipProvider(listenStack);

        sipProvider.addSipListener(this);

    }

    @Override
    public void processRequest(RequestEvent evt) {
        Request req = evt.getRequest();
        String method = req.getMethod();

        if (method.equalsIgnoreCase("SUBSCRIBE")) {
            this.processSubscribeRequest(req, evt);
            return;
        }

        if (method.equalsIgnoreCase("REGISTER")) {
            this.processRegisterRequest(req, evt);
            return;
        }

        if (method.equalsIgnoreCase("PUBLISH")) {
            try {
                Response resp = messageFactory.createResponse(501, req);
                ServerTransaction st = sipProvider.getNewServerTransaction(req);
                st.sendResponse(resp);
            } catch (ParseException | TransactionAlreadyExistsException | TransactionUnavailableException ex) {
                Logger.getLogger(SipLayer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SipException | InvalidArgumentException ex) {
                Logger.getLogger(SipLayer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void processResponse(ResponseEvent responseEvent) {
        try {
            Response resp = responseEvent.getResponse();
            String method = resp.getReasonPhrase();
            System.out.println("Reaseon phrase: " + method.toString());

            Response clonedResponse = (Response) resp.clone();
            clonedResponse.removeFirst(ViaHeader.NAME);

            sipProvider.sendResponse(clonedResponse);

        } catch (SipException ex) {
            Logger.getLogger(SipLayer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NullPointerException ex) {
            Logger.getLogger(SipLayer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void processSubscribeRequest(Request req, RequestEvent evt) {                                  //SEND 200 OK when client subscribes
        try {
            int expires = 180;                                          //Getting value of expires sent in request
            ExpiresHeader exp_received = (ExpiresHeader) req.getExpires();
            if (exp_received != null) {
                expires = exp_received.getExpires();
            }

            Response response = messageFactory.createResponse(200, req);
            ExpiresHeader exp = (ExpiresHeader) headerFactory.createExpiresHeader(expires);                   //Adding expires to subscibe
            response.addHeader(exp);

            ServerTransaction st = sipProvider.getNewServerTransaction(req);
            st.sendResponse(response);

            ContactHeader contact = (ContactHeader) req.getHeader("Contact");
            RequestEventExt ext = (RequestEventExt) evt;

            FromHeader fh = (FromHeader) req.getHeader("From");
            ToHeader th = (ToHeader) req.getHeader("To");

            man.clientSubscribed(ext.getRemoteIpAddress(), contact.getAddress().toString(), ext.getRemotePort(), expires);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void processRegisterRequest(Request req, RequestEvent evt) {
        try {
            Response response = messageFactory.createResponse(200, req);
            ServerTransaction st = sipProvider.getNewServerTransaction(req);
            st.sendResponse(response);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendNotifymessage() throws ParseException {
        try {
            URI uri=null;
            CallIdHeader callid = (CallIdHeader) headerFactory.createCallIdHeader("calllid");
            CSeqHeader cseq = (CSeqHeader) headerFactory.createCSeqHeader(1, "NOTIFY");
            FromHeader from = (FromHeader) headerFactory.createFromHeader(null, null);
            ToHeader to = (ToHeader) headerFactory.createToHeader(null, null);
            List via = null;
            MaxForwardsHeader maxfw = (MaxForwardsHeader) headerFactory.createMaxForwardsHeader(70);
            Request req = messageFactory.createRequest(uri, "NOTOFY", callid, cseq, from, to, via, maxfw);
            
            ClientTransaction ct = sipProvider.getNewClientTransaction(req);
            ct.sendRequest();
            
        } catch (InvalidArgumentException ex) {
            Logger.getLogger(SipLayer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransactionUnavailableException ex) {
            Logger.getLogger(SipLayer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SipException ex) {
            Logger.getLogger(SipLayer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void processTimeout(TimeoutEvent timeoutEvent) {
        System.out.println("Timeout occured");
    }

    @Override
    public void processIOException(IOExceptionEvent exceptionEvent) {
        System.out.println("IOException occured");
    }

    @Override
    public void processTransactionTerminated(TransactionTerminatedEvent transactionTerminatedEvent) {
        System.out.println("Transaction terminated event");
    }

    @Override
    public void processDialogTerminated(DialogTerminatedEvent dialogTerminatedEvent) {
        System.out.println("Dialog Terminated event");
    }
}
