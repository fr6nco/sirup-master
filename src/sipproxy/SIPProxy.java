package sipproxy;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import sipproxy.sipLayer.SipLayer;
import sipproxy.manager.manager;

import java.security.NoSuchAlgorithmException;
import java.util.TooManyListenersException;
import javax.sip.InvalidArgumentException;
import javax.sip.ObjectInUseException;
import javax.sip.PeerUnavailableException;
import javax.sip.TransportNotSupportedException;


public class SIPProxy {

    public static void main(String args[]) {
        try {
            SipLayer sip = SipLayer.getInstance();
            System.out.println("started siplayer");      
            
            manager man = manager.getInstance();
            man.start();
            System.out.println("Manager thread started");
            man.setSipLayer(sip);
            
        } catch (PeerUnavailableException | TransportNotSupportedException | InvalidArgumentException | ObjectInUseException | TooManyListenersException | NoSuchAlgorithmException ex) {   
            System.out.println("Error occured in main class: "  + ex.getMessage()+ "\n");
        }
    }

    
}
