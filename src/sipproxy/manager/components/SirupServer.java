/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sipproxy.manager.components;

/**
 *
 * @author thomas
 */
public class SirupServer {
   
    int id;
    String ipAddress;
    int port;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
    
    public SirupServer(String ipAddress, int port){
        this.ipAddress = ipAddress;
        this.port = port;
    }
    
}
