/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sipproxy.manager.components;

/**
 *
 * @author thomas
 */
public class SirupClient {

    private int id;
    private String ipAddress;
    private int port;
    private String contact;
    private long subscriptionTime;
    private int expires;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public long getSubscriptionTime() {
        return subscriptionTime;
    }

    public void setSubscriptionTime(long subscriptionTime) {
        this.subscriptionTime = subscriptionTime;
    }

    public int getExpires() {
        return expires;
    }

    public void setExpires(int expires) {
        this.expires = expires;
    }
    
    public SirupClient(String ipAddress, int port, String contact, long subscriptionTime, int expires) {
        this.ipAddress = ipAddress;
        this.port = port;
        this.contact = contact;
        this.subscriptionTime = subscriptionTime;
        this.expires = expires;
    }
}
