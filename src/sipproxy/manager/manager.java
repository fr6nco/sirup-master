/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sipproxy.manager;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.TooManyListenersException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sip.InvalidArgumentException;
import javax.sip.ObjectInUseException;
import javax.sip.PeerUnavailableException;
import javax.sip.TransportNotSupportedException;
import sipproxy.api.Api;
import sipproxy.manager.components.*;
import net.sf.json.JSONArray;
import sipproxy.sipLayer.SipLayer;

/**
 *
 * @author thomas
 */
public class manager extends Thread {

    private Api api;
    private SipLayer sip;
    private static manager man = null;
    private List sirupClients;
    private List sirupServers;

    //Singleton initialization
    public static manager getInstance() {
        if (man == null) {
            man = new manager();
        }
        return man;
    }

    //Construct
    private manager() {
        this.api = Api.getInstance();
        this.sirupClients = new ArrayList<SirupClient>();
        this.sirupServers = new ArrayList<SirupServer>();
    }
    
    public void setSipLayer(SipLayer sip) {
        this.sip = sip;
    } 
    
    //Thread

    @Override
    public void run() {
        int i = 0;
        while (true) {
            try {
                this.sleep(100);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
            i++;
            if (i % 100 == 0) {
                sendRandomNotify();
            }
            removeOldEntries();
        }
    }

    private void removeOldEntries() {                       //Removing entries older than 30 sec
        for (int i = 0; i < this.sirupClients.size(); i++) {
            SirupClient sc = (SirupClient) sirupClients.get(i);
            if (System.currentTimeMillis() - sc.getSubscriptionTime() > (sc.getExpires() * 1000)) {
                synchronized (this) {
                    sirupClients.remove(i);
                    i--;
                }
                System.out.println("No subscribe received in 10 sec, removing client");
                //TODO, notify API, subscription timeout
            }
        }
    }
    
    private void sendRandomNotify() {
        for (int i = 0; i < this.sirupClients.size(); i++) {
            try {             
                SirupClient sc = (SirupClient) sirupClients.get(i);
                this.sip.sendNotifymessage();
            } catch (ParseException ex) {
                Logger.getLogger(manager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public synchronized void clientSubscribed(String ip, String contact, int port, int expires) {
        for (int i = 0; i < this.sirupClients.size(); i++) {
            SirupClient cl = (SirupClient) sirupClients.get(i);
            if (cl.getContact().equals(contact) && cl.getIpAddress().equals(ip) && cl.getPort() == port) {
                cl.setSubscriptionTime(System.currentTimeMillis());
                cl.setExpires(expires);
                System.out.println("Client updated, updating subscribe time");
                return;
            }
        }
        SirupClient cl = new SirupClient(ip, port, contact, System.currentTimeMillis(), expires);
        this.sirupClients.add(cl);
        System.out.println("Adding new subscription for client"+ contact +", subscribed for: " + expires+ " seconds");
        //TODO, Notify API - new client subscribed
    }
}
